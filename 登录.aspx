﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="登录.aspx.cs" Inherits="大作业_登录" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <style text="text/css">
    .jumbotron{
        background-color:white;
        height:550px;
        margin-bottom:0px;
        width:1600px;
        
        background-position:right;
        background-size:100%;
        background-image:url(image/c.jpg);

		}
    
      </style>
    <title>登录</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <!-- 导航条 -->
	<nav class="navbar navbar-default  navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a href="#"><span class="navbar-brand">河南</span></a>
				

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			    </button>
			</div>

            <div class="collapse navbar-collapse" id="navbar-collapse">
			
                <button type="button" class="btn btn-primary navbar-btn navbar-right" data-toggle="modal" data-target="#myModal">登录</button>
		</div>
            
		</div>
	</nav>
        <div class="jumbotron">
		<div class="container">
			<div class="page-header">
                </div>
             </div>
           </div>
	
	
	</div>
    
  


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-primary" id="myModalLabel"><span class="glyphicon glyphicon-log-in"></span> 登录</h4>
      </div>
      
       
        <div class="modal-body">
        
           
            <div class="form-group">
                    <asp:Label ID="lblName" runat="server" Text="账号" CssClass=""></asp:Label>
                    <asp:TextBox CssClass="form-control" ID="txtName" runat="server" placeholder="请输入账号"></asp:TextBox>
                </div>
                
                <div class="form-group">
                    <asp:Label ID="lblPS" runat="server" Text="密码" CssClass=""></asp:Label>
                    <asp:TextBox CssClass="form-control" ID="txtPS" runat="server" placeholder="请输入密码" TextMode="Password" ></asp:TextBox>
                </div>
          
        
      </div>
            
      <div class="modal-footer">
          <asp:Button ID="btnLogin" runat="server" Text="登录" CssClass="btn btn-primary" OnClick="btnLogin_Click" />
         <asp:Button ID="btnRegist" runat="server" Text="注册" CssClass="btn btn-default" OnClick="btnRegist_Click" />
      
      </div>
       <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
    </div>
  </div>
</div>
    </form>
    
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    
    
</body>
</html>